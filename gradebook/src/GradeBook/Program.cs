﻿using System;
using System.Collections.Generic;

namespace GradeBook
{
    class Program
    {
        static void Main(string[] args)
        {
            /* ========
             *  Basics.
             * ======== */

            // Arguments.
            if ( args.Length >= 1 )
            {
                System.Console.WriteLine($"Hello, {args[0]}!");
            }
            else
            {
                System.Console.WriteLine("Hello, world!");
            }

            // Array.
            double[] array = new[] {1.1, 2.3, 5.4};
            //
            System.Console.WriteLine("Array:");
            foreach ( var x in array )
            {
                System.Console.WriteLine(x);
            }

            // List (press "Ctrl + ." in Code to autofix error).
            List<double> list = new List<double>() {1.4, 4.3, 6.666};
            list.Add(8.1);
            var listAverage = 0.0;
            //
            foreach ( var x in list )
            {
                listAverage += x;
            }
            listAverage /= list.Count;
            //
            System.Console.WriteLine($"Average number over the list: {listAverage:N2}."); // N2 stands for 2 decimal places in the printed output.

            /* =========
             *  Classes.
             * ========= */

             // We can access classes from the same project, no problem.
             Task task = new Task();
             task.IsRecurring = false; // Use auto property.

             // Hook an event handler.
             // It is also possible to use `-=`, but it is not possible
             // to use direct assignments `=` as this would wipe out
             // all previously assigned delegates (and some could be
             // not yours).
             task.DescriptionChanged += OnTaskDescriptionChanged;
             task.DescriptionChanged -= OnTaskDescriptionChanged;
             task.DescriptionChanged += OnTaskDescriptionChanged;

             // Note from the course: people practicing ASP.NET programming
             // may not even face events of such sort. But it is very typical
             // for desktop programming to have tons of delegates.

             // Try delegate.
             task.SetDescription("Test description");

             try
             {
               task.SetTitle("My first task");
             }
             catch ( ArgumentException ex )
             {
                 System.Console.WriteLine(ex.Message);
                 throw; // Rethrow for someone else, so we give up here.
             }
             catch ( FormatException ex )
             {
                 System.Console.WriteLine(ex.Message);
                 throw; // Rethrow for someone else, so we give up here.
             }
             finally
             {
                 // ... whatever post-mortem code goes here, e.g. close a file/socket.
             }

             System.Console.WriteLine($"Created new task {task.Timestamp}.");

             System.Console.WriteLine( task.ToString() );
        }

        static void OnTaskDescriptionChanged(object sender, EventArgs eventArgs)
        {
            System.Console.WriteLine("OnTaskDescriptionChanged()");
        }
    }
}
