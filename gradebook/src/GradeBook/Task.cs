namespace GradeBook
{
  // Demonstrates abstract classes, polymorphism.
  public interface ITicket
  {
    void SetTitle(string title);
  }

  public class TaskBase
  {
    public TaskBase(string title) : base()
    {
      this.title       = "Untitled";
      this.timestamp   = "ts//";
      this.timestamp  += System.DateTime.Now.ToString("yyyy-MM-dd_HH:mm:ss:ffff");
      this.IsRecurring = false;
    }

    // This method is just to illustrate overloading.
    public void SetTitle(char tc)
    {
      this.SetTitle( tc.ToString() );
    }

    public void SetTitle(string title) // No need to `override` as the method comes from interface.
    {
      // Check for error condition.
      if ( title == "" )
        throw new System.ArgumentException($"The parameter {nameof(title)} is empty string.");

      // A little C# trick to force the programmers fix their diagnostic
      // outputs whenever they change their argument names is nameof() thing.

      this.title = title;
    }

    protected string title;

    protected string timestamp; // The "backing" field for the `Timestamp` property.

    // Demonstrate the use of properties by example of the timestamp.
    // A 'property' thing is a way to encapsulate a member field, i.e.,
    // to carefully control what happens on setting/getting its value.
    public string Timestamp
    {
      get
      {
        return this.timestamp.ToUpper(); // Custom logic on reading the property.
      }
      private set // That's a feature of props: no one can set the timestamp once it's there.
      {
        if ( !string.IsNullOrEmpty(value) )
          this.timestamp = value; // `value` is an implicit variable for properties.
      }
    }

    // There is a shortcut way of defining the properties having no
    // custom logic in setting/getting.
    //
    // CAUTION: properties are not equivalent to public fields. Somewhere in .NET
    //          core these two entities are distinguishable, e.g., when it comes
    //          to serialization. But a more important difference is the ability
    //          to tune access modifiers for getting/setting the value.
    public bool IsRecurring // Whether our task is recurring.
    {
      get;
      set;
    }
  }

  //! Task entity to be placed in a task list.
  public class Task : TaskBase, ITicket /* `public` here is to expose the class to unit testing */
  {
    // `object` is a base type for everything in .NET (even `int`). Then, there is
    // a convention for delegates that most C# developers follow: a convention
    // on the delegate's signature.
    public delegate void DescriptionChangedDelegate(object sender, System.EventArgs args);

    public Task() : base("Untitled")
    {}

    public override string ToString()
    {
      string result = $"Task '{this.title}' from {this.Timestamp}.";
      return result;
    }

    public void SetDescription(string descr)
    {
      // If we have `null` here, it means that no one
      // is listening.
      if ( DescriptionChanged != null )
      {
        // Invoke delegate thus raising the event.
        DescriptionChanged(this, new System.EventArgs());
      }
    }

    // Anyone working with the `Task` class can now use the field
    // `DescriptionChanged` and this is an event.
    public event DescriptionChangedDelegate DescriptionChanged;

    // A readonly field can only be initialized right here or in a ctor.
    public readonly string Author = "Quaoar";

    // A const member is even more strict than readonly. It cannot be
    // initialized in a ctor. Uppercase is a convention. There is no
    // need for .NET to store this field as a state, so it is accessible
    // as a STATIC field, i.e. a class-level field.
    public const string VENDOR = "Mobius";
  }
}
