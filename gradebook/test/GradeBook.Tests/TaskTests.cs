using System;
using Xunit;
using Xunit.Abstractions;

namespace GradeBook.Tests
{
    public class TaskTests
    {
        private readonly ITestOutputHelper output;

        int count = 0;

        public TaskTests(ITestOutputHelper outputHelper)
        {
            this.output = outputHelper;
        }

        // Delegates. We need a level of indirection between our code
        // and some events in the system. We want to have a field that
        // point to any method with a prescribed structure, whatever
        // implementation it comes with (e.g., logging to a console is
        // not different from logging to a file, speaking of interfaces
        // of both).
        public delegate      string          WriteLogDelegate        (string logMessage);
                        /* return type */   /* Delegate name */        /* Parameters*/

        [Fact]
        public void AssertsWork() // Name indicates a "fact" about the software.
        {
            // Where do we look for this `Task` class?
            Task task1 = new Task();

            this.output.WriteLine($"Created task: {task1.ToString()}.");

            // Number of decimal places for floating-point comparison
            // is the 3-rd parameter.
            Assert.Equal(2.5, 2.53, 1);
        }

        [Fact]
        public void WriteLogDelagateCanPointToMethod()
        {
            // We initialize a delegate with a method. It's basically
            // like a pointer to a function.
            WriteLogDelegate log = returnMessage;

            // Even the following is possible, i.e. we have a delegate
            // variable pointing to multiple methods.
            log += returnMessage;
            log += incrementCount;

            var result = log("Hello!");

            Assert.Equal(3, this.count);
        }

        // Here's a method matching the delegate definition.
        string returnMessage(string message)
        {
            count++;
            return message;
        }

        string incrementCount(string message)
        {
            count++;
            return message.ToLower();
        }
    }
}
