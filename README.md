# csharp-fundamentals

This small repo is a code base to follow along with the "C# Fundamentals" course by K. Scott Allen at Pluralsight.com:

https://app.pluralsight.com/library/courses/csharp-fundamentals-dev

Still, instead of just typing what the trainer was doing, I tried to adopt this exercise to achieving something
more interesting for me personally: building up a task management system, such as Maxdone (maxdone.micromiles.co).
This tool is awesome, but it lacks some features I'd like it to have, and also its performance is not ideal. Such
a tool should ideally be deployed in a cloud (like Azure). Also, it looks like a very good example to learn both C# and
ASP.NET technologies.
